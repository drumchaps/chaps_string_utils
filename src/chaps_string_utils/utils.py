# -*- coding: utf-8 -*-

def get_between_substrs(text, sub1, sub2):
    """ Returns the string between the first match of sub1(substring1)
    and sub2(substring2) in the given text/string.
    """
    i1 = text.index(sub1) + len(sub1)
    i2 = text[i1:].index(sub2)
    return text[i1:i2+i1]


